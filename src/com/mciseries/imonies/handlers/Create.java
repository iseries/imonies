package com.mciseries.imonies.handlers;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.system.Utils;
import com.mciseries.imonies.system.Vars;

public class Create {
	public Create(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s, "create")) {
			if(Utils.enoughArgs(s, "/imonies create <account name>", 2, a)) {
				APi.createAccount(a[1]);
				s.sendMessage(Vars.chatPrefix + "Created account \"" + ChatColor.AQUA + a[1] + ChatColor.RESET + "\"");
			}
		}
	}
}
