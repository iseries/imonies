package com.mciseries.imonies.handlers;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.system.Utils;
import com.mciseries.imonies.system.Vars;

public class Pay {
	public Pay(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s, "pay")) {
			if(Utils.enoughArgs(s, "/imonies pay <account name> <amount>", 3, a)) {
				if(Utils.hasAccount(s, a[1])) {
					if(Utils.isNumeric(s, a[2])) {
						if(Utils.isPositive(s, a[2])) {
							if(Utils.has(s, s.getName(), Double.parseDouble(a[2]))) {
								APi.setBalance(s.getName(), APi.getBalance(s.getName()) - Double.parseDouble(a[2]));
								APi.setBalance(a[1], APi.getBalance(a[1]) + Double.parseDouble(a[2]));
								
								s.sendMessage(Vars.chatPrefix + "Paid " + ChatColor.AQUA + a[1] + " " + Utils.format(a[2]));
								if(s.getServer().getPlayer(a[1]) != null)
									s.getServer().getPlayer(a[1]).sendMessage(Vars.chatPrefix + ChatColor.AQUA + s.getName() + ChatColor.RESET + " has paid you " + ChatColor.AQUA + Utils.format(a[2]));
							}
						}
					}
				}
			}
		}
	}
}
