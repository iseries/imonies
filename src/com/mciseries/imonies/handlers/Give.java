package com.mciseries.imonies.handlers;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.system.Utils;
import com.mciseries.imonies.system.Vars;

public class Give {
	public Give(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s, "give")) {
			if(Utils.enoughArgs(s, "/imonies give <account name> <amount>", 3, a)) {
				if(Utils.hasAccount(s, a[1])) {
					if(Utils.isNumeric(s, a[2])) {
						APi.setBalance(a[1], APi.getBalance(a[1]) + Double.parseDouble(a[2]));
						s.sendMessage(Vars.chatPrefix + "Gave " + ChatColor.AQUA + a[2] + ChatColor.RESET + " to " + ChatColor.AQUA + a[1] + ChatColor.RESET + "!");
					}
				}
			}
		}
	}
}
