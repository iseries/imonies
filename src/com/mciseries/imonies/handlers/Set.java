package com.mciseries.imonies.handlers;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.system.Utils;
import com.mciseries.imonies.system.Vars;

public class Set {
	public Set(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s, "set")) {
			if(Utils.enoughArgs(s, "/imonies set <account name> <balance>", 3, a)) {
				if(Utils.hasAccount(s, a[1])) {
					if(Utils.isNumeric(s, a[2])) {
						APi.setBalance(a[1], Double.parseDouble(a[2]));
						s.sendMessage(Vars.chatPrefix + "Set " + ChatColor.AQUA + a[1] + ChatColor.RESET + "'s balance to " + ChatColor.AQUA + a[2] + ChatColor.RESET + "!");
					}
				}
			}
		}
	}
}
