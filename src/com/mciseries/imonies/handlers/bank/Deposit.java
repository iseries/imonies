package com.mciseries.imonies.handlers.bank;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.system.Utils;
import com.mciseries.imonies.system.Vars;

public class Deposit {
	public Deposit(CommandSender s, Command c, String[] a) {
		if(Utils.enoughArgs(s, "/imonies bank deposit <amount>", 3, a)) {
			if(Utils.banksEnabled(s)) {
				if(Utils.bankCommandsEnabled(s)) {
					if(Utils.isNumeric(s, a[2])) {
						if(Utils.isPositive(s, a[2])) {
							if(Utils.has(s, s.getName(), Double.parseDouble(a[2]))) {
								APi.setBalance(s.getName(), APi.getBalance(s.getName()) - Double.parseDouble(a[2]));
								APi.setBankBalance(s.getName(), APi.getBankBalance(s.getName()) + Double.parseDouble(a[2]));
								s.sendMessage(Vars.chatPrefix + "You have deposited " + ChatColor.AQUA + Utils.format(a[2]) + ChatColor.RESET + " into your bank account");
							}
						}
					}
				}
			}
		}
	}
}
