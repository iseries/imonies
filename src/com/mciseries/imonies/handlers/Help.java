package com.mciseries.imonies.handlers;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.imonies.system.Vars;

public class Help {
	public Help(CommandSender s, Command c, String[] a) {
		s.sendMessage(Vars.chatPrefix + "All Commands");
		s.sendMessage(ChatColor.AQUA + "/imonies " + ChatColor.RESET + "- Check your balance.");
		s.sendMessage(ChatColor.AQUA + "/imonies <name> " + ChatColor.RESET + "- Check <name>'s balance.");
		s.sendMessage(ChatColor.AQUA + "/imonies set <name> <balance> " + ChatColor.RESET + "- Set <name>'s balance.");
		s.sendMessage(ChatColor.AQUA + "/imonies give <name> <amount> " + ChatColor.RESET + "- Give <name> money.");
		s.sendMessage(ChatColor.AQUA + "/imonies take <name> <amount> " + ChatColor.RESET + "- Take <name>'s money.");
		s.sendMessage(ChatColor.AQUA + "/imonies pay <name> <amount> " + ChatColor.RESET + "- Pay <amount> to <name>.");
		s.sendMessage(ChatColor.AQUA + "/imonies convert <old eco plugin> " + ChatColor.RESET + "- Convert from old eco plugin.");
		s.sendMessage(ChatColor.AQUA + "/imonies bank deposit <amount> " + ChatColor.RESET + "- Deposit to your bank account.");
		s.sendMessage(ChatColor.AQUA + "/imonies bank withdraw <amount> " + ChatColor.RESET + "- Withdraw from bank account.");
	}
}
