package com.mciseries.imonies.handlers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.iCo6.system.Account;
import com.iCo6.system.Accounts;
import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.system.Utils;
import com.mciseries.imonies.system.Vars;

public class Convert {
	public Convert(CommandSender s, Command c, String[] a) {
		if(Utils.hasPerms(s, "convert")) {
			if(Utils.enoughArgs(s, "/imonies convert <economy plugin>", 2, a)) {
				if(a[1].equalsIgnoreCase("iconomy")) {
					s.sendMessage(Vars.chatPrefix + "Converting to iMonies, server may lag out. Please be patient...");
					Accounts accts = new Accounts();
					for(OfflinePlayer p : Bukkit.getOfflinePlayers()) {
						if(accts.exists(p.getName())) {
							s.sendMessage(Vars.chatPrefix + "Converting " + p.getName() + "'s account...");
							Account acct = new Account(p.getName());
							APi.createAccount(p.getName());
							APi.setBalance(p.getName(), acct.getHoldings().getBalance());
						}
					}
					s.sendMessage(Vars.chatPrefix + "Conversion is complete! Thank you for switching to iMonies!");
				}
				else {
					s.sendMessage(Vars.chatPrefix + ChatColor.RED + "Unfortunately, this economy plugin is not yet supported. Request it on BukkitDev!");
				}
			}
		}
	}
}
