package com.mciseries.imonies;

import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

import com.mciseries.imonies.api.Vault;
import com.mciseries.imonies.listeners.Joins;
import com.mciseries.imonies.system.Conversion;
import com.mciseries.imonies.system.Commands;
import com.mciseries.imonies.system.Interest;
import com.mciseries.imonies.system.files.AccountsFile;
import com.mciseries.imonies.system.files.ConfigFile;
import com.mciseries.imonies.system.files.CreatedAccounts;
import com.mciseries.imonies.system.sql.SQL;

public class Main extends JavaPlugin {
	public void onEnable() {
		ConfigFile.init();
		AccountsFile.init();
		CreatedAccounts.init();
		
		getCommand("imonies").setExecutor(new Commands());
		getServer().getPluginManager().registerEvents(new Joins(), this);
		
		if(ConfigFile.useMySQL)
			SQL.update("ALTER TABLE `" + ConfigFile.mysqlAccountsTable + "` ADD COLUMN `bank_balance` double(10,2) NOT NULL DEFAULT 0.0;");
		else
			SQL.update("ALTER TABLE `" + ConfigFile.mysqlAccountsTable + "` ADD COLUMN `bank_balance` double(10,2);");
		
		Conversion.run();
		
		if(getServer().getServicesManager().getRegistration(Economy.class) != null)
			getServer().getServicesManager().unregister(getServer().getServicesManager().getRegistration(Economy.class).getProvider());
		getServer().getServicesManager().register(Economy.class, new Vault(this), this, ServicePriority.Highest);
		
		Interest.start();
	}
	
	public void onDisable() {
		Interest.stop();
	}
	
	public static void debug(String msg) {
		if(ConfigFile.debug)
			Logger.getLogger("Minecraft").info("[iMonies Debug] " + msg);
	}
}
