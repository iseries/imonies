package com.mciseries.imonies.api;

import java.util.concurrent.ExecutionException;

import com.mciseries.imonies.system.files.ConfigFile;
import com.mciseries.imonies.system.files.CreatedAccounts;
import com.mciseries.imonies.system.sql.SQL;

public class APi {
	public static double getBalance(String name) {
		try {
			return SQL.query("SELECT `balance` FROM `" + ConfigFile.mysqlAccountsTable + "` WHERE lower(name) LIKE '%" + name.toLowerCase() + "%';", "balance");
		} catch (InterruptedException e) {} catch (ExecutionException e) {}
		
		return 0;
	}
	
	public static void setBalance(String name, double bal) {
		SQL.update("UPDATE `" + ConfigFile.mysqlAccountsTable + "` SET `balance`=" + bal + " WHERE lower(name) LIKE '%" + name.toLowerCase() + "%';");
	}
	
	public static double getBankBalance(String name) {
		try {
			return SQL.query("SELECT `bank_balance` FROM `" + ConfigFile.mysqlAccountsTable + "` WHERE lower(name) LIKE '%" + name.toLowerCase() + "%';", "bank_balance");
		} catch (InterruptedException e) {} catch (ExecutionException e) {}
		
		return 0;
	}
	
	public static void setBankBalance(String name, double bal) {
		SQL.update("UPDATE `" + ConfigFile.mysqlAccountsTable + "` SET `bank_balance`=" + bal + " WHERE lower(name) LIKE '%" + name.toLowerCase() + "%';");
	}
	
	public static void createAccount(String name) {
		CreatedAccounts.add(name);
		SQL.update("INSERT INTO `" + ConfigFile.mysqlAccountsTable + "` (`name`, `balance`, `bank_balance`, `owes`) VALUES ('" + name + "', " + ConfigFile.defaultBalance + ", 0.0, 0.0);");
	}
	
	public static boolean hasAccount(String name) {
		return CreatedAccounts.includes(name);
	}
}
