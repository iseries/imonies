package com.mciseries.imonies.api;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;

import com.mciseries.imonies.Main;
import com.mciseries.imonies.system.Utils;
import com.mciseries.imonies.system.files.ConfigFile;

public class Vault implements Economy {
	private final String name = "iMonies";
	private Plugin plugin = null;
	private Server serv = null;
	private Main imonies = null;
	private static final Logger log = Logger.getLogger("Minecraft");
	
	public Vault(Plugin plugin) {
		this.plugin = plugin;
		this.serv = Bukkit.getServer();
		
		serv.getPluginManager().registerEvents(new EconomyServerListener(this), plugin);
		
		if(imonies == null) {
			Plugin imon = plugin.getServer().getPluginManager().getPlugin("iMonies");
			if(imon != null && imon.isEnabled()) {
				imonies = (Main) imon;
				log.info(String.format("[Vault][Economy] %s hooked.", plugin.getDescription().getName(), name));
			}
		}
	}
	
	public boolean isEnabled() {
		if(plugin == null)
			return false;
		return plugin.isEnabled();
	}
	
	public String getName() {
		return name;
	}
	
	public boolean has(String name, String world, double amount) {
		return has(name, amount);
	}
	
	public double getBalance(String name) {
		if(hasAccount(name))
			return APi.getBalance(name);
		
		createPlayerAccount(name);
		return APi.getBalance(name);
	}
	
	public boolean createPlayerAccount(String name) {
		if(hasAccount(name))
			return false;
		
		APi.createAccount(name);
		return true;
	}
	
	public EconomyResponse withdrawPlayer(String name, double amount) {
		double newBalance = getBalance(name) - amount;
		Main.debug("Withdrawing " + amount + " from " + name + " (" + getBalance(name) + " - " + amount + " = " + newBalance + ")");
		if(amount < 0)
			return new EconomyResponse(0, 0, ResponseType.FAILURE, "Cannot withdraw negative funds.");
		
		if(hasAccount(name)) {
			if(has(name, amount)) {
				APi.setBalance(name, newBalance);
				return new EconomyResponse(amount, getBalance(name), ResponseType.SUCCESS, null);
			}
			else {
				return new EconomyResponse(0, 0, ResponseType.FAILURE, "Player does not have enough money.");
			}
		}
		else {
			return new EconomyResponse(0, 0, ResponseType.FAILURE, "Player does not have an account.");
		}
	}
	
	public EconomyResponse depositPlayer(String name, double amount) {
		double newBalance = getBalance(name) + amount;
		Main.debug("Depositing " + amount + " into " + name + " (" + getBalance(name) + " + " + amount + " = " + newBalance + ")");
		if(amount < 0)
			return new EconomyResponse(0, 0, ResponseType.FAILURE, "Cannot deposit negative funds.");
		
		if(hasAccount(name)) {
			if(newBalance > ConfigFile.moneyLimit)
				return new EconomyResponse(0, 0, ResponseType.FAILURE, "Money limit reached (" + Utils.format(ConfigFile.moneyLimit) + ".)");
			
			APi.setBalance(name, newBalance);
			return new EconomyResponse(amount, getBalance(name), ResponseType.SUCCESS, null);
		}
		else {
			return new EconomyResponse(0, 0, ResponseType.FAILURE, "Player does not have an account.");
		}
	}
	
	public String format(double amount) {
		return "" + amount;
	}
	
	public String currencyNameSingular() {
		return "";
	}
	
	public String currencyNamePlural() {
		return "";
	}
	
	public boolean has(String name, double amount) {
		if(!APi.hasAccount(name))
			return false;
		
		return (getBalance(name) >= amount);
	}
	
	public EconomyResponse createBank(String name, String player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "iMonies 4.0 does not yet support banks.");
	}
	
	public EconomyResponse deleteBank(String name) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "iMonies 4.0 does not yet support banks.");
	}
	
	public EconomyResponse bankHas(String name, double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "iMonies 4.0 does not yet support banks.");
	}
	
	public EconomyResponse bankWithdraw(String name, double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "iMonies 4.0 does not yet support banks.");
	}
	
	public EconomyResponse bankDeposit(String name, double amount) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "iMonies 4.0 does not yet support banks.");
	}
	
	public EconomyResponse isBankOwner(String name, String player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "iMonies 4.0 does not yet support banks.");
	}
	
	public EconomyResponse isBankMember(String name, String player) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "iMonies 4.0 does not yet support banks.");
	}
	
	public EconomyResponse bankBalance(String name) {
		return new EconomyResponse(0, 0, ResponseType.NOT_IMPLEMENTED, "iMonies 4.0 does not yet support banks.");
	}
	
	public List<String> getBanks() {
		return new ArrayList<String>();
	}
	
	public boolean hasBankSupport() {
		return false;
	}
	
	public boolean hasAccount(String name) {
		return APi.hasAccount(name);
	}
	
	public boolean hasAccount(String name, String world) {
		return hasAccount(name);
	}
	
	public int fractionalDigits() {
		return 2;
	}
	
	public double getBalance(String name, String world) {
		return getBalance(name);
	}
	
	public EconomyResponse withdrawPlayer(String name, String world, double amount) {
        return withdrawPlayer(name, amount);
    }
	
	public EconomyResponse depositPlayer(String name, String world, double amount) {
        return depositPlayer(name, amount);
    }
	
	public boolean createPlayerAccount(String name, String world) {
		return createPlayerAccount(name);
	}
	
	
	
	public class EconomyServerListener implements Listener {
		Vault v = null;
		
		public EconomyServerListener(Vault v) {
			this.v = v;
		}
		
		@EventHandler(priority = EventPriority.MONITOR)
		public void onPluginEnable(PluginEnableEvent e) {
			if(v.imonies == null) {
				Plugin imon = plugin.getServer().getPluginManager().getPlugin("iMonies");
				
				if(imon != null) {
					v.imonies = (Main) imon;
					log.info(String.format("[Vault][Economy] %s hooked.", plugin.getDescription().getName(), v.name));
				}
			}
		}
		
		@EventHandler(priority = EventPriority.MONITOR)
		public void onPluginDisable(PluginDisableEvent e) {
			if(v.imonies != null) {
				if(e.getPlugin().getDescription().getName().equals("iMonies")) {
					v.imonies = null;
					log.info(String.format("[Vault][Economy] %s unhooked.", plugin.getDescription().getName(), v.name));
				}
			}
		}
	}
}
