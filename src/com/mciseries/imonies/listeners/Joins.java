package com.mciseries.imonies.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.system.files.CreatedAccounts;

public class Joins implements Listener {
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent e) {
		if(!CreatedAccounts.includes(e.getPlayer().getName())) {
			APi.createAccount(e.getPlayer().getName());
			
			if(APi.getBalance(e.getPlayer().getName()) > 0) {}
			else {
				CreatedAccounts.add(e.getPlayer().getName());
			}
		}
	}
}
