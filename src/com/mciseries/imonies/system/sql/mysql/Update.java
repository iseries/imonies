package com.mciseries.imonies.system.sql.mysql;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.mciseries.imonies.Main;
import com.mciseries.imonies.system.files.ConfigFile;
import java.sql.Connection;

public class Update implements Runnable {
	private String query;
	public Update(String query) {
		this.query = query;
	}
	
	public void run() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection conn = DriverManager.getConnection("jdbc:mysql://" + ConfigFile.mysqlIP + ":" + ConfigFile.mysqlPort + "/" + ConfigFile.mysqlDbName + "?user=" + ConfigFile.mysqlUsername + "&password=" + ConfigFile.mysqlPassword);
			Statement s = conn.createStatement();
			s.executeUpdate(query);
			
			s.close();
			conn.close();
			
			Main.debug("Did an updating query: " + query);
		} catch (InstantiationException e) {
			Main.debug(e.getMessage());
		} catch (IllegalAccessException e) {
			Main.debug(e.getMessage());
		} catch (ClassNotFoundException e) {
			Main.debug(e.getMessage());
		} catch (SQLException e) {
			Main.debug(e.getMessage());
		}
	}
}
