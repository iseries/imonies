package com.mciseries.imonies.system.sql.mysql;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.Callable;

import com.mciseries.imonies.Main;
import com.mciseries.imonies.system.files.ConfigFile;
import java.sql.Connection;

public class Query implements Callable<Double> {
	private String query, col;
	public Query(String query, String col) {
		this.query = query;
		this.col = col;
	}
	
	public Double call() throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection conn = DriverManager.getConnection("jdbc:mysql://" + ConfigFile.mysqlIP + ":" + ConfigFile.mysqlPort + "/" + ConfigFile.mysqlDbName + "?user=" + ConfigFile.mysqlUsername + "&password=" + ConfigFile.mysqlPassword);
			Statement s = conn.createStatement();
			
			ResultSet r = s.executeQuery(query);
			double d = 0;
			
			if(r.next())
				d = r.getDouble(col);
			
			s.close();
			conn.close();
			r.close();
			
			Main.debug("Did query: " + query + " to get " + d);
			
			return d;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0.0;
	}
}