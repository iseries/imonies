package com.mciseries.imonies.system.sql.sqlite;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import java.sql.Connection;

import com.mciseries.imonies.Main;

public class Update implements Runnable {
	private String query;
	public Update(String query) {
		this.query = query;
	}
	
	public void run() {
		try {
			Main.debug("Queried: " + query);
			Class.forName("org.sqlite.JDBC").newInstance();
			Connection conn = DriverManager.getConnection("jdbc:sqlite:plugins/iMonies/Accounts.db");
			Statement s = conn.createStatement();
			s.executeUpdate(query);
			
			s.close();
			conn.close();
		} catch (InstantiationException e) {
			Main.debug(e.getMessage());
		} catch (IllegalAccessException e) {
			Main.debug(e.getMessage());
		} catch (ClassNotFoundException e) {
			Main.debug(e.getMessage());
		} catch (SQLException e) {
			Main.debug(e.getMessage());
		}
	}
}
