package com.mciseries.imonies.system.sql.sqlite;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.Callable;

import java.sql.Connection;

public class Query implements Callable<Double> {
	private String query, col;
	public Query(String query, String col) {
		this.query = query;
		this.col = col;
	}
	
	public Double call() throws Exception {
		Class.forName("org.sqlite.JDBC").newInstance();
		Connection conn = DriverManager.getConnection("jdbc:sqlite:plugins/iMonies/Accounts.db");
		Statement s = conn.createStatement();
		
		ResultSet r = s.executeQuery(query);
		double d = r.getDouble(col);
		
		conn.close();
		s.close();
		r.close();
		
		return d;
	}
}
