package com.mciseries.imonies.system.sql;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.mciseries.imonies.system.files.ConfigFile;

public class SQL {
	public static double query(String query, String col) throws InterruptedException, ExecutionException {
		if(ConfigFile.useMySQL) {
			ExecutorService exec = Executors.newFixedThreadPool(1);
			Callable<Double> call = new com.mciseries.imonies.system.sql.mysql.Query(query, col);
			Future<Double> future = exec.submit(call);
			exec.shutdown();
			
			return future.get();
		}
		else {
			ExecutorService exec = Executors.newFixedThreadPool(1);
			Callable<Double> call = new com.mciseries.imonies.system.sql.sqlite.Query(query, col);
			Future<Double> future = exec.submit(call);
			exec.shutdown();
			
			return future.get();
		}
	}
	
	public static void update(String query) {
		if(ConfigFile.useMySQL) {
			(new Thread(new com.mciseries.imonies.system.sql.mysql.Update(query))).run();
		}
		else {
			(new Thread(new com.mciseries.imonies.system.sql.sqlite.Update(query))).run();
		}
	}
}
