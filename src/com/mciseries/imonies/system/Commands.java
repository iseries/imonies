package com.mciseries.imonies.system;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.handlers.*;
import com.mciseries.imonies.handlers.bank.*;
import com.mciseries.imonies.system.files.ConfigFile;

public class Commands implements CommandExecutor {
	public boolean onCommand(CommandSender s, Command c, String str, String[] a) {
		if(a.length == 0) {
			s.sendMessage(Vars.chatPrefix + "Your balance: " + ChatColor.AQUA + Utils.format(APi.getBalance(s.getName())));
			if(ConfigFile.useBanks)
				s.sendMessage(Vars.chatPrefix + "Your bank balance: " + ChatColor.AQUA + Utils.format(APi.getBankBalance(s.getName())));
		}
		else {
			if(a[0].equalsIgnoreCase("set")) {
				new Set(s, c, a);
			}
			else if(a[0].equalsIgnoreCase("pay")) {
				new Pay(s, c, a);
			}
			else if(a[0].equalsIgnoreCase("give")) {
				new Give(s, c, a);
			}
			else if(a[0].equalsIgnoreCase("take")) {
				new Take(s, c, a);
			}
			else if(a[0].equalsIgnoreCase("help")) {
				new Help(s, c, a);
			}
			else if(a[0].equalsIgnoreCase("convert")) {
				new Convert(s, c, a);
			}
			else if(a[0].equalsIgnoreCase("create")) {
				new Create(s, c, a);
			}
			else if(a[0].equalsIgnoreCase("bank")) {
				if(a.length == 1) {
					s.sendMessage(Vars.chatPrefix + ChatColor.RED + "Unknown bank command! Try /imonies help");
					return true;
				}
				if(a[1].equalsIgnoreCase("deposit")) {
					new Deposit(s, c, a);
				}
				else if(a[1].equalsIgnoreCase("withdraw")) {
					new Withdraw(s, c, a);
				}
				else {
					s.sendMessage(ChatColor.RED + "Unknown bank command! Try /imonies help");
				}
			}
			else {
				if(APi.hasAccount(a[0])) {
					s.sendMessage(Vars.chatPrefix + ChatColor.AQUA + a[0] + "'s " + ChatColor.RESET + "balance: " + ChatColor.AQUA + Utils.format(APi.getBalance(a[0])));
				}
				else {
					s.sendMessage(Vars.chatPrefix + ChatColor.RED + "Unknown command or account! Try /imonies help");
				}
			}
		}
		return true;
	}
}
