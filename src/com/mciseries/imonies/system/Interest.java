package com.mciseries.imonies.system;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.system.files.ConfigFile;

public class Interest {
	public static ScheduledExecutorService service = null;
	public static ScheduledFuture<?> interest = null;
	
	public static void start() {
		service = Executors.newScheduledThreadPool(1);
		interest = service.scheduleAtFixedRate(new Runnable() {
			public void run() {
				if(ConfigFile.useInterest) {
					for(Player p : Bukkit.getOnlinePlayers()) {
						p.sendMessage(Vars.chatPrefix + "You just got " + ChatColor.AQUA + getAmount(p) + ChatColor.RESET + " for being on " + getTime() + "!");
						APi.setBalance(p.getName(), APi.getBalance(p.getName()) + getAmount(p));
					}
				}
			}
		}, ConfigFile.interestInterval, ConfigFile.interestInterval, TimeUnit.SECONDS);
	}
	
	private static double getAmount(Player p) {
		if(!ConfigFile.interestPercentile) {
			return ConfigFile.interestAmount;
		}
		else {
			return (APi.getBalance(p.getName()) / 100) * ConfigFile.interestAmount;
		}
	}
	private static String getTime() {
		return String.format("%d minutes and %d seconds", TimeUnit.SECONDS.toMinutes(ConfigFile.interestInterval), ConfigFile.interestInterval - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(ConfigFile.interestInterval)));
	}
	
	public static void stop() {
		interest.cancel(true);
		service.shutdownNow();
	}
}
