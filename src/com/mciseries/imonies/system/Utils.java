package com.mciseries.imonies.system;

import java.text.DecimalFormat;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.system.files.ConfigFile;


public class Utils {
	public static boolean sqliteInUse = false;
	
	public static String format(double amount) {
		return format("" + amount);
	}
	public static String format(String amount) {
		DecimalFormat format = new DecimalFormat("#,###");
		format.setMinimumFractionDigits(2); format.setMaximumFractionDigits(2);
		amount = format.format(Double.parseDouble(amount));
		
		amount = amount.replace(".00", "");
		
		amount = amount.replace(".", ConfigFile.centsSeparator);
		amount = amount.replace(",", ConfigFile.thousandsSeparator);
		
		return amount;
	}
	
	public static boolean hasPerms(CommandSender s, String p) {
		if(s.hasPermission("imonies." + p)) {
			return true;
		}
		s.sendMessage(Vars.chatPrefix + ChatColor.DARK_RED + "You don't have permission for this!");
		return false;
	}
	
	public static boolean enoughArgs(CommandSender s, String u, int r, String[] a) {
		if(a.length == r) {
			return true;
		}
		s.sendMessage(Vars.chatPrefix + ChatColor.RED + "Usage: " + u);
		return false;
	}
	
	public static boolean hasAccount(CommandSender s, String p) {
		if(APi.hasAccount(p)) {
			return true;
		}
		s.sendMessage(Vars.chatPrefix + ChatColor.RED + "That account does not exist!");
		return false;
	}
	
	public static boolean isNumeric(CommandSender s, String n) {
		try {
			Double.parseDouble(n);
			return true;
		} catch (NumberFormatException e) {
			s.sendMessage(Vars.chatPrefix + ChatColor.RED + n + " must be a number!");
			return false;
		}
	}
	
	public static boolean has(CommandSender s, String a, double b) {
		if(APi.getBalance(a) >= b) {
			return true;
		}
		s.sendMessage(Vars.chatPrefix + ChatColor.RED + "You do not have enough money!");
		return false;
	}
	
	public static boolean bankHas(CommandSender s, String a, double b) {
		if(APi.getBankBalance(a) >= b) {
			return true;
		}
		s.sendMessage(Vars.chatPrefix + ChatColor.RED + "Your bank account does not have enough money!");
		return false;
	}
	
	public static boolean isPositive(CommandSender s, String str) {
		return isPositive(s, Double.parseDouble(str));
	}
	
	public static boolean isPositive(CommandSender s, double num) {
		if(num > 0.0) {
			return true;
		}
		s.sendMessage(Vars.chatPrefix + ChatColor.RED + num + " must be positive!");
		return false;
	}
	
	public static boolean banksEnabled(CommandSender s) {
		if(ConfigFile.useBanks) {
			return true;
		}
		s.sendMessage(Vars.chatPrefix + ChatColor.DARK_RED + "Banks are not enabled!");
		return false;
	}
	
	public static boolean bankCommandsEnabled(CommandSender s) {
		if(ConfigFile.allowBankCommandUse) {
			return true;
		}
		s.sendMessage(Vars.chatPrefix + ChatColor.DARK_RED + "Bank commands are not enabled, find a bank sign instead!");
		return false;
	}
}