package com.mciseries.imonies.system.files;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.mciseries.imonies.Main;

public class ConfigFile {
	private static File conFile = new File("plugins/iMonies/Config.yml"), folder = new File("plugins/iMonies");
	private static FileConfiguration conf = YamlConfiguration.loadConfiguration(conFile);
	
	public static boolean debug, useMySQL, useInterest, interestPercentile, useBanks, allowBankCommandUse, allowBankSignUse;
	public static double defaultBalance, moneyLimit, interestAmount;
	public static String mysqlAccountsTable, mysqlUsername, mysqlPassword, mysqlIP, mysqlDbName, centsSeparator, thousandsSeparator;
	public static int mysqlPort;
	public static long interestInterval;
	
	public static void init() {
		try {
			if(!folder.exists())
				folder.mkdirs();
			if(!conFile.exists()) {
				conFile.createNewFile();
			}
		} catch (IOException e) {
			Main.debug("Error created config file/folder: " + e);
		}
		
		setDefault("Debug", false); debug = conf.getBoolean("Debug");
		setDefault("MySQL.Use", false); useMySQL = conf.getBoolean("MySQL.Use");
		setDefault("DefaultBalance", 100D); defaultBalance = conf.getDouble("DefaultBalance");
		setDefault("MySQL.AccountsTable", "accounts"); mysqlAccountsTable = conf.getString("MySQL.AccountsTable");
		setDefault("MySQL.Username", "root"); mysqlUsername = conf.getString("MySQL.Username");
		setDefault("MySQL.Password", "123"); mysqlPassword = conf.getString("MySQL.Password");
		setDefault("MySQL.Port", 3306); mysqlPort = conf.getInt("MySQL.Port");
		setDefault("MySQL.IP", "localhost"); mysqlIP = conf.getString("MySQL.IP");
		setDefault("MySQL.DbName", "Accounts"); mysqlDbName = conf.getString("MySQL.DbName");
		setDefault("Formatting.CentsSeparator", "."); centsSeparator = conf.getString("Formatting.CentsSeparator");
		setDefault("Formatting.ThousandsSeparator", ","); thousandsSeparator = conf.getString("Formatting.ThousandsSeparator");
		setDefault("MoneyLimit", 1000000D); moneyLimit = conf.getDouble("MoneyLimit");
		setDefault("Interest.Interval", 3600L); interestInterval = conf.getLong("Interest.Interval");
		setDefault("Interest.Amount", 100D); interestAmount = conf.getDouble("Interest.Amount");
		setDefault("Interest.Use", false); useInterest = conf.getBoolean("Interest.Use");
		setDefault("Interest.Percentile", false); interestPercentile = conf.getBoolean("Interest.Percentile");
		setDefault("Banks.Use", false); useBanks = conf.getBoolean("Banks.Use");
		setDefault("Banks.AllowCommandUse", true); allowBankCommandUse = conf.getBoolean("Banks.AllowCommandUse");
		setDefault("Banks.AllowSignUse", true); allowBankSignUse = conf.getBoolean("Banks.AllowSignUse");
	}
	
	private static void setDefault(String key, Object value) {
		if(conf.getString(key) == null) {
			conf.set(key, value);
			save();
		}
	}
	
	private static void save() {
		try {
			conf.save(conFile);
		} catch (IOException e) {
			Main.debug("Error saving config file: " + e);
		}
	}
}
