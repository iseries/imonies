package com.mciseries.imonies.system.files;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class CreatedAccounts {
	private static File conFile = new File("plugins/iMonies/CreatedAccounts.yml");
	private static FileConfiguration conf = YamlConfiguration.loadConfiguration(conFile);
	
	public static void init() {
		try {
			if(!conFile.exists()) {
				conFile.createNewFile();
				add("NPEfailsafe");
			}
		} catch (IOException e) {
			// we'll handle these later!
		}
	}
	
	public static void add(String player) {
		List<String> newList = conf.getStringList("Created");
		newList.add(player.toLowerCase());
		
		conf.set("Created", newList);
		
		save();
	}
	
	public static boolean includes(String player) {
		return conf.getStringList("Created").contains(player.toLowerCase());
	}
	
	
	private static void save() {
		try {
			conf.save(conFile);
		} catch (IOException e) {
			// we'll handle these later
		}
	}
}
