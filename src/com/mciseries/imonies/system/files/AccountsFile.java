package com.mciseries.imonies.system.files;

import java.io.File;
import java.io.IOException;

import com.mciseries.imonies.system.sql.SQL;

public class AccountsFile {
	private static File conFile = new File("plugins/iMonies/Accounts.db");
	
	public static void init() {
		try {
			if(!conFile.exists() && !ConfigFile.useMySQL) {
				conFile.createNewFile();
			}
		} catch (IOException e) {}
		
		if(!ConfigFile.useMySQL)
			SQL.update("CREATE TABLE IF NOT EXISTS `" + ConfigFile.mysqlAccountsTable + "` (`name` varchar(255) NOT NULL, `balance` double(10,2) NOT NULL, `bank_balance` double(10,2), `owes` double(10,2));");
		else
			SQL.update("CREATE TABLE IF NOT EXISTS `" + ConfigFile.mysqlAccountsTable + "` (`name` varchar(255) PRIMARY KEY NOT NULL, `balance` double(10,2) NOT NULL DEFAULT 0.0, `bank_balance` double(10,2) NOT NULL DEFAULT 0.0, `owes` double(10,2) NOT NULL DEFAULT 0.0);");
	}
}
