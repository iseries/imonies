package com.mciseries.imonies.system;

import java.util.concurrent.ExecutionException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.mciseries.imonies.Main;
import com.mciseries.imonies.api.APi;
import com.mciseries.imonies.system.sql.SQL;

public class Conversion {
	public static void run() {
		Main.debug("Converting all old bank accounts to the new table...");
		for(Player p : Bukkit.getOnlinePlayers()) {
			double oldBal = 0;
			try {
				oldBal = SQL.query("SELECT `balance` FROM `bank` WHERE lower(name) LIKE `%" + p.getName() + "%`;", "balance");
			} catch (InterruptedException e) {} catch (ExecutionException e) {}
			
			if(oldBal > 0) {
				APi.setBankBalance(p.getName(), oldBal);
				SQL.update("UPDATE `bank` SET `balance`=0 WHERE lower(name) LIKE `%" + p.getName() + "%`;");
			}
		}
		
		Main.debug("If everyone has their bank balance back, you should delete that useless 'bank' table!");
	}
}
